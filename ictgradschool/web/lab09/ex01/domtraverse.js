"use strict";

/* Your answer here */
var firstDiv = document.getElementsByClassName("text1")[0];
firstDiv.style.backgroundColor = "pink";

var lastP = document.getElementById("footer");
lastP.style.color = "red";

var theThingToRemove = document.getElementsByClassName("text2")[0];
var theParent = theThingToRemove.parentNode;
theParent.removeChild(theThingToRemove);

var firstP = document.getElementsByTagName("p")[0];
firstP.innerText = "Hello World";

var button = document.getElementById("makeBold");
button.onclick= makeBold;

function makeBold (){
    var allParagraph = document.getElementsByTagName("p");
    var len = allParagraph.length;
    for (var i=0; i<len;i++){
        allParagraph[i].style.fontWeight= "bold";
    }

}